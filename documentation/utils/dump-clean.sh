#!/bin/bash

# exit in case of any error
set -e

FILE="$1"
if [ -z "$FILE" ]; then
	echo "Usage:"
	echo "  $0 file.sql"
	exit 1
fi

# stripping e-mail addresses
sed "s/'[a-z\.\-]*@[a-z\.\-]*'/NULL/g" -i "$FILE"

# stripping passwords (now are SHA1 salted)
sed "s/'[a-f0-9]\{40\}'/NULL/g" -i "$FILE"
