# Credits and Licenses

For obvious reasons every image in the "partner" directory should be considered as provided by their authors only for the Linux Day in Turin, as default.

Contact them to receive information about their copyright status and their logo policies.
