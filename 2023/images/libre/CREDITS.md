# Credits and licenses

Credits and licenses for each file in this directory.

* `ils-logo-circle-text.svg`
    * https://commons.wikimedia.org/wiki/File:Italian_Linux_Society_logo_text_penguin_circle_black.svg
    * Virginia Foti. Penguin originally designed by Roberto Guido., CC0, da Wikimedia Commons
