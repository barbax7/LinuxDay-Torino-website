# Linux Day Torino's website
![Linux Day Torino](https://raw.githubusercontent.com/0iras0r/ld2016/master/2016/static/linuxday-64.png)

Content Management System for the __Linux Day Torino__ event, but just from 2016 to 2023 (and beyond we hope!).

The [Linux Day](https://www.linuxday.it/) is a yearly Italian event were Italian cities organize free conferences for everyone, talking about GNU/Linux, Free software, open source, and we meet amazing new friends.

The [Linux Day Torino](https://linuxdaytorino.org/) is just our local implementation, and this project is the related website.

## Introduction

This project is a content management system for multiple conferences handled by similar users. It allows completly different themes for each conference.

This project is designed using GNU/Linux, Apache/nginx, PHP, MySQL/MariaDB and the [suckless-php](https://github.com/valerio-bozzolan/suckless-php) framework.

We use PHP. Yes. We don't care about your complaints. PHP is a powerful hypertext preprocessor available since 1995.

This project tries to rely on simple Debian stable packages. We have no third party dependencies. We use just a stupid framework made by us (10 files). No Composer. No Laravel. No Megabytes of NodeJS modules1. Very low RAM footprint. Very low CPU utilization. Please try it!

## Quick start (option 1: Docker)

Copy-paste these commands to have the website up in seconds:

```
git clone https://gitlab.com/LinuxDayTorino/LinuxDay-Torino-website.git/
git clone https://gitlab.com/valerio.bozzolan/suckless-php LinuxDay-Torino-website/includes/suckless-php

cd LinuxDay-Torino-website
cp documentation/docker/load-example.php load.php
cp documentation/apache/htaccess.conf .htaccess.conf

docker-compose up
```

Your website will be here:

http://localhost:8080/

You can then use Docker Compose normally.

## Quick start (option 2: Vagrant)

To start hacking on this project you can also use Vagrant to reproduce the production environment quickly. You should know how to install this package from your favourite GNU/Linux distribution. Then, inside the repository, just run:

	vagrant up

Then, follow the instructions from your terminal.

## Quick start on Arch Linux (or anything other than Debian GNU/Linux)

The virtual machine does not work with VirtualBox. It is also incompatible with Vagrant rsync synced folders. You have to install libvirt, any packages required for NFS and firewalld because libvirt requires a firewall for no apparent reason and the firewall *has* to be firewalld. Which by default blocks libvirt machines from accessing NFS.

You need a few packages and a Vagrant plugin:

```bash
pacman -S nfs-utils libvirt firewalld
vagrant plugin install vagrant-libvirt
```

Then, to start the VM:

```bash
systemctl start libvirtd firewalld nfs-server rpcbind
firewall-cmd --zone=libvirt --add-service=nfs
firewall-cmd --zone=libvirt --add-service=mountd
firewall-cmd --zone=libvirt --add-service=rpc-bind
vagrant up --provider=libvirt
```

The `firewall-cmd` commands can be made permanent with `--permanent`.

## Bare-metal installation

If you don't like the quick starts using Docker or Vagrant, you can just install your LAMP (GNU/Linux, Apache2/nginx, MySQL/MariaDB, PHP) and run this project as you want.

### Bare-metal installation (Debian GNU/Linux)

On a Debian GNU	/Linux `stable` system (actually tested on `stretch`) install a webserver (Apache or nginx, is your choice) and a MariaDB database server and some other additional packages:

    apt install apache2 mariadb-server php php-mysql php-mbstring libapache2-mod-php gettext libjs-jquery libjs-leaflet libmarkdown-php
    a2enmod rewrite
    a2enconf javascript-common
    service apache2 reload

Place the project files somewhere, available to your webserver:

	cd /var/www
	git clone https://gitlab.com/LinuxDayTorino/LinuxDay-Torino-website.git/ linuxday
	git clone https://github.com/valerio-bozzolan/suckless-php linuxday/includes/suckless-php

Copy the example [Apache configuration file](documentation/apache/htaccess.conf) and save it as `.htaccess` (e.g. `/var/www/linuxday/.htaccess`). Or if you want nginx obviously we have an [nginx configuration example](documentation/nginx/locations.conf).

Remember to change the `DocumentRoot` of your Apache configuration file to respect your pathname (e.g. `/var/www/linuxday`).

Create a MariaDB database. Example:

	CREATE DATABASE ldto CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

Create a dedicated user for that database. Example (change the password!):

	CREATE USER ldto@localhost IDENTIFIED BY 'changemeasd!';
	GRANT ALL PRIVILEGES ON ldto.* TO ldto@localhost;

Then copy the [load-example.php](load-example.php) in a new file called `/var/www/linuxday/load.php` and type there your database credentials.

Populate your database with the file [documentation/database/database-schema.sql](documentation/database/database-schema.sql). Example:

	sudo mysql ldto < documentation/database/database-schema.sql

### Hardening a bare-metal installation

The website can be kept in read-only for the webserver user, and nothing more. E.g.:

    chown root:www-data -R /var/www/linuxday
    chmod o=            -R /var/www/linuxday

You may also want to declare the PHP `open_basedir` directive to the value of `/var/www/linuxday:/usr/share/php` and nothing more to restrict the access to just these files.

Also you can disable all the unused system calls like `exec()` etc.

### Installation in a custom subdirectory

If you want to serve the website from a subdirectory, you can.

In Apache HTTPd, change this line in your `.htaccess` file:

	# RewriteBase /
	RewriteBase /linuxday

Also change this line from your base file `load.php`:

	# define( 'ROOT', '' );
	define( 'ROOT', '/linuxday' );

## Update source code from remote

To update the source code from remote using Docker:

	git pull
	cd includes/suckless-php && git pull && cd -

To update the source code from remote using Vagrant:

	git pull
	cd includes/suckless-php && git pull && cd -
	vagrant provision

## Reset database from remote

To reset your database to the latest version, in Docker just run:

	git pull
	docker-compose rm
	docker-compose start

To reset your database to the latest version, in Vagrant just run:

	git pull
	vagrant provision

To reset your database in a bare metal, just import this file in your MariaDB:

[documentation/database/database-schema.sql](documentation/database/database-schema.sql).

## Database export

To export the database using Docker:

	docker-compose run db  mysqldump --extended-insert=FALSE ldto -hdb -p'changemeasd!asd!' > ./documentation/database/database-schema.sql
	docker-compose run web ./documentation/utils/dump-clean.sh                                ./documentation/database/database-schema.sql

To export the database using Vagrant:

	vagrant ssh
	/vagrant/Vagrant/pull-database.sh          /vagrant/documentation/database/database-schema.sql
	/vagrant/documentation/utils/dump-clean.sh /vagrant/documentation/database/database-schema.sql
	exit

To export the database in bare metal, just run `mysqldump` as usual.

### API

The website exposes some REST-ful APIs.

The one called _tagliatella_ generates a valid XML document containing all the talks/events (in a format that someone call Pentabarf, but it's not the Pentabarf - really, this stupid format has not even an official name). The _tagliatella_ API gives an HTTP Status Code 500 and some other uncaught exceptions if an error raises.

The one called _tropical_ generates a valid `iCal` document to import an Event or a Conference in your favourite calendar client.

## Internationalization

The website interface (plus some parts of the content) is internationalized thanks to GNU Gettext. GNU Gettext is both a software and a standard. This is not the place to learn GNU Gettext, but this is how you can use it.

### Translate a string 

You can edit the `.po` file with Poedit to translate some strings to a language:

* [l10n/en_US.utf8/LC_MESSAGES/linuxday.po](l10n/en_US.utf8/LC_MESSAGES/linuxday.po) (English)
* [l10n/pms.utf8/LC_MESSAGES/linuxday.po](l10n/pms.utf8/LC_MESSAGES/linuxday.po) (Piedmontese)

To apply language changes with Docker Compose:

	docker-compose run web ./l10n/localize.sh .

To apply language changes in Vagrant:

	vagrant provision

### Translate a new language

Copy the [GNU Gettext template](l10n/linuxday.pot) in a new pathname similar to the English one, and also rename it to the `.po` extension.

Then in the [includes/load-post.php](includes/load-post.php) file you can add another `register_language()` line.

## Backend

The login page should be there:

http://localhost:8080/2016/login.php

To access the backend using Docker, create an user first:

	docker-compose run web ./cli/add-user.php --uid=admin --pwd=admin --role=admin

To access the backend using Vagrant, just follow the instructions on your terminal.

## Update Docker Hub

To update this Docker image:

https://hub.docker.com/r/italianlinuxsocietytorino/linuxdaytorinowebsite

Contact gnu at linux dot it.

That user can run these:

```
docker build -t italianlinuxsocietytorino/linuxdaytorinowebsite:latest .
docker push     italianlinuxsocietytorino/linuxdaytorinowebsite:latest
```

Anyway, you can just build for yourself and use your local build.

## License

(c) 2015 Roberto Guido, Linux Day Torino website contributors

(C) 2016-2019 [Valerio Bozzolan](https://boz.reyboz.it/), [Ludovico Pavesi](https://github.com/lvps), [Rosario Antoci](https://linuxdaytorino.org/2019/user/oirasor), Roberto Guido, Linux Day contributors

(C) 2020-2022 Roberto Guido, Linux Day Torino website contributors

(C) 2023 Valerio Bozzolan, Rosario Antoci, Roberto Guido, Linux Day Torino website contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License](LICENSE.md) for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

Thank you for forking this project!

Contributions are welcome! If you do any non-minor contribution you are welcome to put your name in the copyright header of your touched file.
